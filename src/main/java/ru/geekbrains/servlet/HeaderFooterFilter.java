package ru.geekbrains.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Фильтры
 *
 * Для решения ряда задач в Servlet API предусмотрен механизм фильтров, которые могут
 * обрабатывать запрос до того как он будет обработан каким-либо из работающих на сервере
 * сервлетов. Основное применение фильтров
 * - проверка авторизации клиента и регулирования доступа к ресурсам (Spring Security
 * реализован как фильтр)
 * - добавление какого-то общего контента в ответы всех сервлетов (заголовок и подвал
 * страниц).
 *
 * Для создания фильтра необходимо создать класс, который реализует интерфейс
 * javax.servlet.Filter. Чтобы этот класс был использован контейнером сервлетов как фильтр его
 * нужно отметить аннотацией @WebFilter или прописать в файле web.xml.
 *
 * Если фильтр добавлен при помощи аннотации, то не существует способа указать его место в
 * цепочке среди других фильтров.
 *
 * По умолчанию любой пришедший на сервер HTTP запрос один раз проходит через цепочку
 * фильтров. В случае если он был перенаправлен при помощи forward или include фильтры к
 * нему вновь не применяются, но это поведение можно изменить при помощи опции
 * dispatcher, которую можно указать как в файле web.xml так и в аннотации @WebFilter.
 * Возможные значения этой опции:
 * ● REQUEST - по умолчанию
 * ● FORWARD - каждый forward запроса будет проходить через такой фильтр
 * ● INCLUDE - каждый include запроса будет проходить через такой фильтр
 * ● ERROR - каждое перенаправление на страницу ошибки будет проходить через такой фильтр
 * Пример:
 * @WebFilter(urlPatterns = "/*", dispatcherTypes = {DispatcherType.REQUEST, * DispatcherType.FORWARD})
 */

//@WebFilter(urlPatterns = "/*")
public class HeaderFooterFilter implements Filter {
    private transient FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws
            ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws IOException, ServletException {
        filterConfig.getServletContext().
                getRequestDispatcher("/header.html").include(req, resp);
        chain.doFilter(req, resp);
        filterConfig.getServletContext().
                getRequestDispatcher("/footer.html").include(req, resp);
    }

    @Override
    public void destroy() {
    }
}
