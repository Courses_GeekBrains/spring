package ru.geekbrains.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.geekbrains.models.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

@WebServlet(name = "ProductServlet", urlPatterns = "/product_servlet")
public class ProductServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(FirstServlet.class);
    private static final String CONTENT_TYPE = "text/html; charset=windows-1251";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("New GET request");
        resp.setContentType(CONTENT_TYPE);
        // Определить тип браузера из которого был отправлен запрос
        //logger.info("User agent: {}", req.getHeader("User-agent"));
        // Установление типа контента и его кодировку
        //resp.setHeader("Content-Type", "text/html; charset=windows-1251");
        resp.getWriter().printf("<h1>Список продуктов</h1>");

        resp.getWriter().printf("<ul>");
        resp.getWriter().printf("<li>" + new Product(1, "Молоко", "50") + "</li>");
        resp.getWriter().printf("<li>" + new Product(2, "Масло", "200") + "</li>");
        resp.getWriter().printf("<li>" + new Product(3, "Сахар", "350") + "</li>");
        resp.getWriter().printf("<li>" + new Product(4, "Сметана", "65") + "</li>");
        resp.getWriter().printf("<li>" + new Product(5, "Творог", "422") + "</li>");
        resp.getWriter().printf("<li>" + new Product(6, "Хлеб", "39") + "</li>");
        resp.getWriter().printf("<li>" + new Product(7, "Печенье", "80") + "</li>");
        resp.getWriter().printf("<li>" + new Product(8, "Шоколад", "222") + "</li>");
        resp.getWriter().printf("<li>" + new Product(9, "Рыба", "546") + "</li>");
        resp.getWriter().printf("<li>" + new Product(10, "Пиво", "965") + "</li>");
        resp.getWriter().printf("</ul>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger.info("New POST request");
        //resp.getWriter().printf("<h1>New POST request</h1>");
        resp.getWriter().printf("<h1>New POST request with body %s</h1>", readAllLines(req.getReader()));
    }

    public String readAllLines(BufferedReader reader) throws IOException {
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            content.append(line);
            content.append(System.lineSeparator());
        }
        return content.toString();
    }
}