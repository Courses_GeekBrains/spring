package ru.geekbrains.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * класс, реализующий интерфейс javax.servlet.http.HttpServlet
 * <p>
 * javax.servlet.http.HttpServlet  реализует интерфейс javax.servlet.Servlet
 */
@WebServlet(name = "FirstServlet", urlPatterns = "/first_servlet")
public class FirstServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(FirstServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("New GET request");
        // Определить тип браузера из которого был отправлен запрос
        logger.info("User agent: {}", req.getHeader("User-agent"));
        // Установление типа контента и его кодировку
        //resp.setHeader("Content-Type", "text/html; charset=utf-8");
        resp.getWriter().printf("<h1>New GET request</h1>");

        resp.getWriter().printf("<h1>New GET request with parameters:<br> param1 = %s;<br> param2 = %s </h1>",
                req.getParameter("param1"),
                req.getParameter("param2"));

        /**
         * Cookies
         *
         * У сервлетов есть возможность сохранять определенную информацию на стороне клиента.
         * Для этого используются маленькие текстовые файлы, которые принято называть cookies.
         * Именно в таком файле сохраняется идентификатор текущей сессии пользователя. При
         * необходимости мы можем сохранять в файлах и свою информацию.
         * Для работы с файлами cookies следует использовать класс Cookies а также
         *
         * Содержимое созданного файла будет передано на клиент через заголовок запроса с именем
         * Cookies. После чего этот файл будет сохранен браузером в специальной директории для
         * временных файлов. При каждом новом запросе от данного клиента файл cookie будет
         * считываться из этой временной директории и передаваться на сервер в заголовке запроса с
         * именем Cookies.
         *
         * Для cookie нет метода аналогичного getParameter или getAttribute. Чтобы найти конкретный
         * cookie по имени требуется проитерировать массив, который возвращает метод getCookies().
         */
//        Cookie cookie = new Cookie("user", "someUserName");
//        // Удалить файл cookie можно установив ему нулевое время жизни.
//        cookie.setMaxAge(0);
//        resp.addCookie(cookie);

        /**
         * Redirect
         *
         * Redirect - это перенаправление средствами протокола HTTP. При этом способе
         * перенаправления на запрос клиента отправляется ответ с кодом 30x (чаще всего 301 - moved
         * permanently). Согласно спецификации протокола HTTP при получении такого ответа клиент
         * должен перейти по ссылке, которая содержится в заголовке ответа с именем ‘Location’. Это
         * ссылка указывает, куда был перемещен ресурс.
         * Такая переадресация делается при помощи метода sendRedirect() из интерфейса
         * HttpServletResponse. В качестве параметра этому методу передается ссылка на ресурс, на
         * который требуется переадресация. Ссылка может быть как относительной (переадресация
         * на другой ресурс сервера, на котором расположено приложение), так и абсолютной (любой
         * другой ресурс, доступный по сети).
         */
        // Пример переадресации по относительной ссылке
        // обратите внимание, что здесь используется ссылка относительно URL сервера
        //resp.sendRedirect(req.getContextPath() + "/basic_servlet");
        /**
         * Пример переадресации по абсолютной ссылке
         *
         * Важно отметить, что при данном способе переадресации клиент получает от сервера ссылку
         * на новый ресурс через HTTP-ответ и доступ к этому ресурсу осуществляется через новый
         * HTTP-запрос. По этой причине передача параметров из исходного запроса в
         * переадресованный возможна либо через GET-параметры в ссылке переадресации либо,
         * либо через атрибуты сессии. Атрибуты из HttpServletRequest в этом случае использовать
         * нельзя.
         */
        //resp.sendRedirect("https://ya.ru");

        /**
         * Forward
         *
         * Переадресация средствами протокола HTTP бывает удобна далеко не во всех случаях,
         * поэтому контейнер сервлетов предоставляет механизм forward, который позволяет
         * переадресовывать запрос между сервлетами внутри сервера приложения без использования
         * дополнительных HTTP запросов. Для выполнения forward-переадресации необходимо
         * прежде всего вызвать метод getRequestDispatcher() из интерфейса ServetContext(), который
         * вернет реализацию интерфейса RequestDispatcher. В качестве параметра этому методу
         * следует передать имя сервлета или относительную ссылку статический файл из папки
         * webapp. Далее нужно вызывать метод forward() этого интерфейса.
         *
         * Следует отметить, что RequestDispatcher полученный из контекста сервлета можно
         * использовать только для переадресации по абсолютной ссылке. В случае если требуется
         * переадресация по относительной ссылке следует использовать точно такой же метод из
         * объекта HTTP-запроса. Он поддерживает как абсолютные так и относительные ссылки.
         *
         * Пример переадресации на другой сервлет. Обратите внимание, что в данном случае мы
         * можем передавать дополнительную информацию в запрос через добавление/изменение его
         * атрибутов и эта информация может быть использована в том сервлете на который идет
         * переадресация.
         */
//        req.setAttribute("newAttr", "attrValue");
//        // Обратите внимание, что здесь указывается имя сервлета
        //getServletContext().getRequestDispatcher("/basic_servlet").forward(req, resp);
        //getServletContext().getRequestDispatcher("/index.html").forward(req, resp);

        /**
         * Include
         *
         * Иногда нам требуется собрать ответ на запрос из результатов которые возвращаются
         * несколькими сервлетами или содержатся в нескольких статических файлах. Для подобных
         * случаев в интерфейсе RequestDispatcher имеется метод include(), который вызывается так же
         * как и forward().
         *
         * Следует отметить, что если include осуществляет переадресацию на другой сервлет, то будет
         * запрещен ряд действий над объектом ответа на запрос. Можно выполнять любые действия с
         * атрибутами или телом ответа, но нельзя изменять заголовки, файлы cookies или статус
         * ответа.
         */
        logger.info("New Get request with includes");
        getServletContext().getRequestDispatcher("/header.html").include(req, resp);
        resp.getWriter().println("<p>Response body from servlet</p>");
        getServletContext().getRequestDispatcher("/footer.html").include(req, resp);

        /**
         * Сессии
         *
         * Обратите внимание, что здесь значение указывается в секундах, а не в минутах, как в файле
         * web.xml. Также следует помнить о том, что некоторые сервера приложений поддерживают
         * установку времени жизни сессии с точностью до минуты. В этом случае заданное в секундах
         * время округляется до минут в меньшую сторону. Такой особенностью обладает Apache
         * TomCat.
         */
        /* HttpSession sessionObj = req.getSession(true);
        sessionObj.setMaxInactiveInterval(10 * 60);*/
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger.info("New POST request");
        //resp.getWriter().printf("<h1>New POST request</h1>");
        resp.getWriter().printf("<h1>New POST request with body %s</h1>", readAllLines(req.getReader()));
    }

    public String readAllLines(BufferedReader reader) throws IOException {
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            content.append(line);
            content.append(System.lineSeparator());
        }
        return content.toString();
    }
}
