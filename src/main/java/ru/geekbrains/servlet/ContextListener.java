package ru.geekbrains.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Слушатели (Listeners)
 *
 * Через слушателей в Servlet API реализован механизм обработки событий. Вот список
 * интерфейсов, при помощи которых могут быть реализованы слушатели для различных событий
 * - ServletRequestListener - отслеживание создания и обработки HTTP запроса
 * - ServletRequestAttributeListener - отслеживание действий с атрибутами запросов
 * - ServletContextListener - отслеживание создания и уничтожения контекста сервлетов (фактически всего приложения)
 * - ServletContextAttributeListener - отслеживание действий с атрибутами сервлетов
 * - HttpSessionListener - отслеживание создания и уничтожения классов сессий
 * - HttpSessionAttributeListener - отслеживание действий с атрибутами сессии
 * - HttpSessionBindingListener - отслеживание привязки объектов к сессии
 * - HttpSessionActivationListener - отслеживание активации и пассивации сессии
 * (в случае если сессии сохраняются во внешнее хранилище)
 *
 * Класс-слушатель для создания JDBC соединение при создании приложения на сервере приложений
 */
@WebListener
public class ContextListener implements ServletContextListener {
    private Logger logger = LoggerFactory.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        logger.info("Context initialization: " + context.getContextPath());

        String jdbcConnectionString = context.getInitParameter("jdbcConnectionString");
        String username = context.getInitParameter("dbUsername");
        String password = context.getInitParameter("dbPassword");
        if (isNotNullOrEmpty(jdbcConnectionString) || isNotNullOrEmpty(username)) {
            logger.error("Connection string and DB username must be specified");
            return;
        }

        try {
            Connection connection =
                    DriverManager.getConnection(jdbcConnectionString, username, password);
            context.setAttribute("jdbcConnection", connection);
        } catch (SQLException e) {
            logger.error("", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        Connection conn = (Connection) context.getAttribute("jdbcConnection");
        if (conn == null) {
            return;
        }
        try {
            conn.close();
        } catch (SQLException e) {
            logger.error("", e);
        }
    }

    private boolean isNotNullOrEmpty(String str) {
        return str != null && str.isEmpty();
    }
}
