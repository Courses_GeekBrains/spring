package ru.geekbrains.models;

public class Product {
    private long id;
    private String title;
    private String cost;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Product() {
    }

    public Product(long id, String title, String cost) {
        this.id = id;
        this.title = title;
        this.cost = cost;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(id).append(".");
        sb.append(" ").append(title).append(":");
        sb.append(" ").append(cost);
        return sb.toString();
    }
}
