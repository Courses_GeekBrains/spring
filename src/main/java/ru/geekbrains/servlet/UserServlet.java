package ru.geekbrains.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "JdbcServlet", urlPatterns = "/jdbc_servlet")
public class UserServlet extends HttpServlet {
    private Logger logger = LoggerFactory.getLogger(UserServlet.class);
    private Connection conn;

    /**
     * Прикрыли метод init() который будет вызван при создании сервлета. В нем мы получаем
     * из контекста класс соединения с БД, который в дальнейшем можем использовать для
     * доступа к БД при обработке HTTP запросов.
     *
     * @throws ServletException
     */

    @Override
    public void init() throws ServletException {
        ServletContext context = getServletContext();
        conn = (Connection) context.getAttribute("jdbcConnection");
        if (conn == null) {
            throw new ServletException("No JDBC connection in Servlet Context");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse
            resp) throws ServletException, IOException {
        logger.info("Get all tables");
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM pg_catalog.pg_tables;");
            while (rs.next()) {
                String tableName = rs.getString(2);
                resp.getWriter().println("<p> " + tableName + "</p>");
            }
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }
}
